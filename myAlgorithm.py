#(c) PyBoKids

import config
import sys
sys.path.insert(0, './realMBot')
sys.path.insert(0, './simulatedMBot')
sys.path.insert(0, '/usr/local/lib/python2.7/comm')

#import comm
import easyiceconfig as EasyIce
import time

from mBotReal import MBotReal
from mBotGazebo import MBotGazebo

if __name__ == "__main__":
	# Cargamos fichero desde .cfg:
	ic = EasyIce.initialize(sys.argv)
	props = ic.getProperties()
	propPrefix = "PyBoKids"

	# Carga de ROBOT y PUERTO
	robot = props.getProperty(propPrefix + ".Robot")
        if not robot:
            raise Exception("PyBoKids-ERROR: no se pudo establecer tipo de robot")

	puerto = props.getProperty(propPrefix + ".Puerto")
        if not puerto:
            raise Exception("PyBoKids-ERROR: no se pudo establecer puerto de conexion con el robot")

	if robot == "mBotReal":
 		myPyBoKids = MBotReal(puerto)

	elif robot == "mBotGazebo":
		myPyBoKids = MBotGazebo()

	# En myPyBoKids tendre la instancia de my objeto, para trabajar con el, independientemente del HW
	myPyBoKids.quienSoy()
	print ("A continuacion se ejecuta su codigo con el robot", myPyBoKids.tipo)

	# TODO: type your code here! (This is just an example)
	myPyBoKids.avanzar(200)
	time.sleep(2)
	myPyBoKids.parar()

	myPyBoKids.girarIzquierda(200)
	time.sleep(4.1)
	myPyBoKids.parar()
	myPyBoKids.avanzar(200)
	time.sleep(2)
	myPyBoKids.parar()

	myPyBoKids.girarIzquierda(200)
	time.sleep(4.1)
	myPyBoKids.parar()
	myPyBoKids.avanzar(200)
	time.sleep(2)
	myPyBoKids.parar()

	myPyBoKids.girarIzquierda(200)
	time.sleep(4.1)
	myPyBoKids.parar()
	myPyBoKids.avanzar(200)
	time.sleep(2)
	myPyBoKids.parar()

	myPyBoKids.girarIzquierda(200)
	time.sleep(4.1)
	myPyBoKids.parar()
	myPyBoKids.avanzar(200)
	time.sleep(2)
	myPyBoKids.parar()

