#(c) PyBoKids

# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '/usr/local/lib/python2.7/parallelIce')
from motors import Motors
from PyBoKids import PyBoKids
import Ice

class simulatedMBot:
    '''
    Controlador para el Robot Mbot Simulado de PyBoKids
    '''
    def __init__(self):
        PyBoKids.__init__(self)
        self._tipo = "simulatedMBot"
        props = Ice.createProperties()
        props.setProperty("PyBoKids.Motors.Proxy", "Motors:default -h localhost -p 9999")
        props.setProperty("PyBoKids.Motors.maxV", "1")
        props.setProperty("PyBoKids.Motors.maxW", "0.5")
        id = Ice.InitializationData()
        id.properties = props
        ic = Ice.initialize(id)
        self.motors = Motors (ic, "PyBoKids.Motors")

    def avanzar(self, vel):
        '''
        Función que hace avanzar al robot en línea recta a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de avance del robot (máximo 255)
        '''
        v = vel * self.motors.maxV / 255
        self.motors.setV(v)
        self.motors.setW(0)
        self.motors.sendVelocities()

    def retroceder(self, vel):
        '''
        Función que hace retroceder al robot en línea recta a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de retroceso del robot (máximo 255)
        '''
        v = vel * self.motors.maxV / 255
        self.motors.setV(-v)
        self.motors.setW(0)
        self.motors.sendVelocities()

    def parar(self):
        '''
        Función que hace detenerse al robot.

        '''
        self.motors.setV(0)
        self.motors.setW(0)
        self.motors.sendVelocities()

    def girarIzquierda(self, vel):
        '''
        Función que hace rotar al robot sobre sí mismo hacia la izquierda a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de giro del robot (máximo 255)
        '''

        v = vel * self.motors.maxW / 255
        self.motors.setV(0)
        self.motors.setW(v)
        self.motors.sendVelocities()

    def girarDerecha(self, vel):
        '''
        Función que hace rotar al robot sobre sí mismo hacia la derecha a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de giro del robot (máximo 255)
        '''

        v = vel * self.motors.maxW / 255
        self.motors.setV(0)
        self.motors.setW(-v)
        self.motors.sendVelocities()

    def move(self, velV, velW):
        '''
        Función que hace avanzar y girar al robot al mismo tiempo, según las velocidades V,W dadas como parámetro.

        @type velV, velW: entero
        @param velV, velW: velocidades de avance y giro del robot (máximo 255)
        '''
        v = velV * self.motors.maxV / 255
        w = velW * self.motors.maxW / 255

        self.motors.setV(v)
        self.motors.setW(w)
        self.motors.sendVelocities()

    def quienSoy(self):
        print ("Yo soy un robot simulado simulatedMBot")

    @property
    def tipo(self):
        return self._tipo

    @tipo.setter
    def tipo(self, valor):
        self._tipo = valor
