#(c) PyBoKids

# -*- coding: utf-8 -*-

from .extra import dec2Bin
from PyMata.pymata import PyMata

READ = True
WRITE = False

class Multiplexer:
	"""Multiplexores para PyMata.

	Esta clase sirve como plantilla para los multiplextores digital y analógico,
	no puede usarse como tal. Deben usarse DigitalMultiplexer o AnalogMultiplexer.

	Puede estar en modo Lectura os escritura, por defecto está en modo lectura

	"""

	def __init__(self, selectors, z, board):
		"""Contructor del Multiplexor.

		Parametros:
		  selectors -> array con los pines en enteros de los selectores (A, B, C, ....)
		  z -> pin de entrada/salida del multiplexor
		  board -> placa donde se localiza el multiplexor

		"""

		self._selectors = selectors
		self._z = z
		self._board = board

		self._mode = READ

		self._val = 0

		for i in selectors:
			self._board.set_pin_mode(i, PyMata.OUTPUT, PyMata.DIGITAL)


	def __str__(self):
		return "Multiplexer: z: " + str(self._z) +  "selectors: " + str(selectors)

	def selectPin(self, num):
		"""Selecciona el Pin indicado.

		Parametros:
		  num -> pin a seleccionar

		"""
		bits = dec2Bin(num,len(self._selectors))
		for i in range(len(self._selectors)):
			self._board.digital_write(self._selectors[i], bits[i])

	def getValue(self):
		return self._val


	def geValueAt(self, num):
		"""Devuelve el valor del Pin indicado.

		Parametros:
		  num -> pin a seleccionar

		"""
		self.selectPin(num)
		return self.getValue()

	def setValue(self, value):
		pass

	def setValueAt(self, num, value):
		"""Escribe el valor en el Pin indicado.

		Parametros:
		  num -> pin a seleccionar
		  value -> valor a escribir

		"""
		self.selectPin(num)
		self.setValue(value)

	def isModeRead(self):
		return self._mode

	def isModeWrite(self):
		return not self._mode

	def changeMode(self):
		self._mode = not self._mode

	def _cb_z(self, dat):
		"""Callback para almacenar la lectura del pin seleccionado.

		Parametros:
		  dat -> lectura del pin, el valor esta en dat[2]

		"""
		self._val = dat[2]




class DigitalMultiplexer (Multiplexer):
	"""Multiplexor digital para PyMata.

	Permite gestionar tanto multiplexores como demultiplexores digitales con Pymata
	Hereda de multimplexer

	Puede estar en modo Lectura os escritura, por defecto está en modo lectura


	"""

	def __init__(self, selectors, z, board):
		"""Contructor de DigitalMultiplexer.

		Parametros:
		  selectors -> array con los pines en enteros de los selectores (A, B, C, ....)
		  z -> pin de entrada/salida del multiplexor
		  board -> placa donde se localiza el multiplexor

		"""

		Multiplexer.__init__(self, selectors, z, board)
		self._board.set_pin_mode(self._z, PyMata.INPUT, PyMata.DIGITAL, self._cb_z)
	def __str__(self):
		return "DigitalMultiplexer: z: " + str(self._z) +  "selectors: " + str(selectors)


	def setValue(self, value):
		self._board.digital_write(self._z, value)

	def changeMode(self):
		self._mode = not self._mode
		if (self._mode == READ):
			self._board.set_pin_mode(self._z, PyMata.INPUT, PyMata.DIGITAL, self._cb_z)
		else:
			self._board.set_pin_mode(self._z, PyMata.OUTPUT, PyMata.DIGITAL)




class AnalogMultiplexer (Multiplexer):
	"""Multiplexor Analógico para PyMata.

	Permite gestionar tanto multiplexores como demultiplexores analógicos con Pymata
	Hereda de multimplexer

	Puede estar en modo Lectura os escritura, por defecto está en modo lectura


	"""

	def __init__(self, selectors, z, board):
		"""Contructor de AnalogMultiplexer.

		Parametros:
		  selectors -> array con los pines en enteros de los selectores (A, B, C, ....)
		  z -> pin de entrada/salida del multiplexor
		  board -> placa donde se localiza el multiplexor

		"""

		Multiplexer.__init__(self, selectors, z, board)
		self._board.set_pin_mode(self._z, PyMata.INPUT, PyMata.ANALOG, self._cb_z)

	def __str__(self):
		return "AnalogMultiplexer: z: " + str(self._z) +  "selectors: " + str(selectors)


	def setValue(self, value):
		self._board.analog_write(self._z, value)

	def changeMode(self):
		self._mode = not self._mode
		if (self._mode == READ):
			self._board.set_pin_mode(self._z, PyMata.INPUT, PyMata.ANALOG, self._cb_z)
		else:
			self._board.set_pin_mode(self._z, PyMata.OUTPUT, PyMata.ANALOG)



