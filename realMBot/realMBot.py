#(c) PyBoKids

# -*- coding: utf-8 -*-

import signal
from time import sleep
import sys
from .makeblock.lib.mBot import *
from threading import Lock, Event
from .sensorsThread import ThreadSensor
from PyBoKids import PyBoKids

class dibujo:
    def __init__(self, l, width, x, y):
        self.l = l
        self.width = width
        self.px = x
        self.py = y


class realMBot (mBot):
    '''
    Controlador para el Robot Mbot de PyBoKids-Arduino
    '''
    def __init__(self, port = None):
        '''
        Constructor de la clase realMBot. Crea una instancia del controlador del robot.

        @type port: texto
        @param port: puerto serie (USB) en el que está conectado el robot(en Windows: "COMX", en Linux: "/dev/ttyAMCX"). si se deja vacio, usa el pincho 2.4G

        '''
        self._tipo = "realMBot"
        mBot.__init__(self)

        self._boton = 0
        self.lockBut = Lock()

        self._potenciometro = 0
        self.lockPot = Lock()

        self._ultrasonido = 400 # lectura maxima del utrasonido
        self.lockUS = Lock()

        self._irr = 0 #este valor el mando nunca le devuelve
        self.lockIRR = Lock()

        self._irsiguelineas = -1
        self.lockSL = Lock()

        self._sonido = 0
        self.lockSon = Lock()

        self._luzPlaca = 0
        self.lockLP = Lock()

        self._usingHID = False

        if (port):
            self.startWithSerial(port)
        else:
            self.startWithHID()
            self._usingHID = True

            self.kill_event = Event()

            self.thread = ThreadSensor(self, self.kill_event)
            self.thread.daemon = True

        sleep(5) # necesario para dar tiempo a que se inicie la conexion con el robot

        self.tones ={"ZERO":0,"C3":131,"D3":147,"E3":165,"F3":175,"G3":196,"A3":220,"B3":247,
                "C4":262,"D4":294,"E4":330,"F4":349,"G4":392,"G4S":415,"A4":440,"A4S":455,"B4":494,
                "C5":523,"C5H":554,"D5":587,"D5S":622,"E5":659,"F5":698,"F5S":740,"G5":784,"G5S":830,"A5":880,"B5":988}

        self.dibujos={"COMPLETO":dibujo([255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255], 16, 0, 0),
                        "FLECHA_DERECHA":dibujo([0,0,0,24,24,24,24,24,24,24,90,60,24,0,0,0], 16, 0, 0),
                        "FLECHA_IZQUIERDA":dibujo([0,0,0,24,60,90,24,24,24,24,24,24,24,0,0,0], 16, 0, 0),
                        "FLECHA_ARRIBA":dibujo([0,0,0,0,0,32,64,255,255,64,32,0,0,0,0,0], 16, 0, 0),
                        "FLECHA_ABAJO":dibujo([0,0,0,0,0,4,2,255,255,2,4,0,0,0,0,0], 16, 0, 0),
                        "OJOS_ABIERTOS":dibujo([0,24,36,66,66,36,24,0,0,24,36,66,66,36,24,0], 16, 0, 0),
                        "OJOS_CERRADOS":dibujo([0,8,4,2,2,4,8,0,0,8,4,2,2,4,8,0], 16, 0, 0),
                        "CORAZON":dibujo([0,0,0,0,48,72,68,34,34,68,72,48,0,0,0,0], 16, 0, 0),
                        "CIRCULO":dibujo([0,0,0,0,60,66,129,129,129,129,66,60,0,0,0,0], 16, 0, 0),
                        "CRUZ":dibujo([0,0,0,0,129,195,102,60,24,60,102,195,129,0,0,0], 16, 0, 0)}
        self.dict_mando={0: "null", 69:"A", 70:"B", 71:"C", 68:"D", 67: "E", 13:"F",
                        64:"ARRIBA", 7:"IZQUIERDA", 9:"DERECHA", 25: "ABAJO", 21: "AJUSTES",
                        22:"0", 12:"1", 24:"2", 94:"3", 8:"4", 28:"5", 90:"6", 66:"7", 82:"8", 74:"9"}
        self.iniciar()

    # if client is stopped you can not start again, Threading.Thread raised error
    def iniciar(self):
        if self._usingHID:
            self.kill_event.clear()
            self.thread.start()

    # if client is stopped you can not start again
    def detener(self):
        if self._usingHID:
            self.kill_event.set()

    def prueba (self):
        print ("!")

    def encenderLedPlaca(self, indice, r = 255, g = 255, b = 255):
        '''
        Función que enciende un led específico de la placa del robot.

        @type indice : entero
        @type r : entero
        @type g : entero
        @type b : entero

        @param indice: número de LED que se desea encender (0 -- enciende ambos, 1 -- enciende el derecho, 2 -- enciende el izquierdo)
        @param r: intensidad de color rojo [0-255]
        @param g: intensidad de color verde [0-255]
        @param b: intensidad de color azul [0-255]
        '''
        self.doRGBLedOnBoard(indice,r,g,b)

    def encenderLed(self, puerto, indice, r = 255, g = 255, b = 255):
        '''
        Función que enciende un led específico externo a la placa del robot.

        @type puerto : entero
        @type indice : entero
        @type r : entero
        @type g : entero
        @type b : entero

        @param puerto: puerto al que están enchufados los leds
        @param indice: número de LED que se desea encender
        @param r: intensidad de color rojo [0-255]
        @param g: intensidad de color verde [0-255]
        @param b: intensidad de color azul [0-255]
        '''
        self.doRGBLed(puerto,2,indice,r,g,b)

    def apagarLedPlaca(self, indice):
        '''
        Función que apaga un led específico de la placa del robot.

        @type indice : entero
        @param indice: número de LED que se desea encender (0 -- apaga ambos, 1 -- apaga el derecho, 2 -- apaga el izquierdo)
        '''
        self.doRGBLedOnBoard(indice,0,0,0)

    def apagarLed(self, puerto, indice):
        '''
        Función que apaga un led específico de la placa del robot.

        @type puerto : entero
        @type indice : entero
        @param puerto: puerto al que están enchufados los leds
        @param indice: número de LED que se desea apagar
        '''
        self.doRGBLed(puerto, 2, indice,0,0,0)

    def escribirTexto(self, puerto, texto, dx = 0, dy = 7, brillo = 3):
        '''
        Función que escribe texto en el panel led.

        @type puerto: entero
        @type texto: text
        @type dx: entero
        @type dy: entero
        @type brillo: entero

        @param puerto: Puerto del panel Led
        @param texto: texto a escribir
        @param dy: fila inferior donde empezar a escribir
        @param dx: columna inzquierda donde empezar a escribir
        @param brillo: brillo de los leds
        '''
        self.doTextLedMX(puerto, texto, dx, dy, brillo)

    def escribirFrase(self, puerto, texto, brillo = 3):
        '''
        Función que escribe texto en el panel led con desplazamiento para ver la frase entera.

        @type puerto: entero
        @type texto: text
        @type brillo: entero

        @param puerto: Puerto del panel Led
        @param texto: texto a escribir
        @param brillo: brillo de los leds
        '''
        #usa 6 columnas por caracter
        # 16 -> número de columnas del panel led
        l = len(texto) * -6 + 16
        i = -1

        self.doTextLedMX(puerto, texto, 0, 7, brillo)
        while (i >= l):
            self.doTextLedMX(puerto, texto, i, 7, brillo)
            sleep(0.1)
            i = i - 1

    def dibujosPosibles(self):
        '''
        Función que Devuelve una lista con los nombres de los dibujos predefinios.

        @return lista con posibles dibujos
        '''
        return list(self.dibujos.keys())

    def pintarDibujo(self, puerto, nombreDibujo, brillo = 4):
        '''
        Función que Permite pintar en el panel led Dibujos predefinidos.

        @type puerto: entero
        @type nombreDibujo: text
        @type brillo: entero

        @param puerto: Puerto del panel Led
        @param nombreDibujo: Nombre del dibujo predefinido
        @param brillo: brillo de los leds
        '''
        dib = self.dibujos[nombreDibujo]

        self.dibujar(puerto, dib.l, dib.px, dib.py, brillo, dib.width)

    def dibujar (self, puerto, dibujo, posicionX=0, posicionY=0, brillo= 4, ancho= 16):
        '''
        Función permite dibujar en el panel led. Para dibujar se le pasa una lista que representa
        el panel led. Cada columna esta representada por un entero en binario Empezando por abajo.
        por ejempo, si encendemos el primero, el cuarto y el octavo: 1 + 8 + 128 = 137
        @type puerto: entero
        @type dibujo: lista
        @type brillo: entero
        @type ancho: entero
        @type posicionX: entero
        @type posicionY: entero
        @param puerto: Puerto del panel Led
        @param dibujo: lista que representa el dibujo (leds que se encienden)
        @param posicionX: columna donde empieza el dibujo
        @param posicionY : fila donde empieza el dibujo
        @param brillo: brillo de los leds
        @param ancho: ancho en columnas del dibujo
        '''
        self.doBMPLedMX(puerto, ancho, posicionX, posicionY, dibujo, brillo)

    def escribirReloj(self, puerto, hora, minuto, brillo = 3):
        '''
        Función que escribe un reloj digital en el panel led.

        @type puerto: entero
        @type hora: entero
        @type minuto: entero
        @type brillo: entero

        @param puerto: Puerto del panel Led
        @param hora: horas
        @param minuto: minutos
        @param brillo: brillo de los leds
        '''
        self.doClockLedMX(puerto, hora, minuto, brillo)

    def borrarMatriz(self, puerto):
        '''
        Función que borra el panel led.
        @type puerto: entero
        @param puerto: Puerto del panel Led
        '''
        self.doTextLedMX(puerto, " ", 0, 0, 0)

    def leerBoton(self):
        '''
        Función que retorna si el botón ha sido pulsado o no en formato numérico.
        Devuelve 0 si el botón no se ha pulsado y 1 en caso contrario.

        '''
        if not self._usingHID:
            self.requestButtonOnBoard(128, _cb_boton)

        self.lockBut.acquire()
        b = self._boton
        self.lockBut.release()

        return b

    def playTono(self, tono, tiempo):
        '''
        Función que reproduce un tono musical utilizando el zumbador integrado en el robot.
        @type tono: string
        @type tiempo: entero
        @param tono: tono que se quiere reproducir como texto.
             "C3":131,"D3":147,"E3":165,"F3":175,"G3":196,"A3":220,"B3":247,
                "C4":262,"D4":294,"E4":330,"F4":349,"G4":392,"A4":440,"B4":494,
                "C5":523,"D5":587,"E5":659,"F5":698,"G5":784,"A5":880,"B5":988
        @param tiempo: tiempo que dura el tono en milisegundos.
        '''
        self.doBuzzer(tono, tiempo)

    def leerUltrasonido(self):
        '''
        Función que devuelve el valor leído por el sensor de ultrasonidos del robot.

        '''
        if not self._usingHID:
            self.requestUltrasonicSensor(122,3, _cb_ultrasonido)

        self.lockUS.acquire()
        us = self._ultrasonido
        self.lockUS.release()

        return us

    def valoresMandoPosibles(self):
        '''Devuelve una lista con los valores posibles del mando (para escribirlos luego bien).'''
        return list(self.dict_mando.values())

    def leerMandoIR(self):
        if not self._usingHID:
            self.requestIROnBoard(1,_cb_irr)

        self.lockIRR.acquire()
        irr = self._irr
        self.lockIRR.release()
        code = self.dict_mando[irr]
        return code

    def leerIntensidadLuz(self):
        '''
        Función que devuelve el valor leído por el sensor de intensidad de luz del robot.
        '''
        if not self._usingHID:
            self.requestLightOnBoard(130, _cb_luzPLaca)
        self.lockLP.acquire()
        lp = self._luzPlaca
        self.lockLP.release()
        return lp

    def leerIRSigueLineas(self):
        '''
        Función que devuelve el valor leído por el sensor de IR del siguelíneas.

        @return num: Valor devuelto por el sensor:
                            3 - no está sobre una línea,
                            2 -- IR derecho sobre línea,
                            1 -- IR izquierdo sobre línea,
                            0 -- Ambos IR sobre línea
        '''
        if not self._usingHID:
            self.requestLineFollower(123,2, _cb_irSiguelineas)
        self.lockSL.acquire()
        dat = self._irsiguelineas
        self.lockSl.release()
        return dat

    def leerIntensidadSonido(self):
        '''
        Función que devuelve el valor leído por el sensor de ultrasonidos del robot.

        '''
        if not self._usingHID:
            self.requestSoundSensor(124,4, _cb_sonido)
        self.lockSon.acquire()
        dat = self._sonido
        self.lockSon.release()
        return dat

    def leerPotenciometro(self):
        '''
        Función que devuelve el valor leído por el sensor de ultrasonidos del robot.
        '''
        if not self._usingHID:
            self.requestPotentiometerSensor(125,4,_cb_potenciometro)
        self.lockSL.acquire()
        dat = self._potenciometro
        self.lockSl.release()
        return dat

    def moverServo(self, *args):
        '''
        Función que hace girar al servo motor a un angulo dado como parámetro.
        @type args: lista
        @param args: lista de argumentos:
                        args[0]: puerto al que esta conectado el controlador del servo
                        args[1]: banco al que esta conectado el servo en el controlador
                        args[2]: angulo de giro del servo. 0-180 grados. ¡PROBAR GIRO ANTES DE MONTAR EL SERVO!
        '''
        puerto = args[0]
        banco = args[1]
        angulo = args[2]
        self.doServo(puerto, banco, angulo)

    def avanzar(self, vel):
        '''
        Función que hace avanzar al robot en línea recta a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de avance del robot (máximo 255)
        '''
        self.doMove(vel, vel)

    def retroceder(self, vel):
        '''
        Función que hace retroceder al robot en línea recta a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de retroceso del robot (máximo 255)
        '''
        self.doMove(-vel, -vel)

    def parar(self):
        '''
        Función que hace detenerse al robot.

        '''
        self.doMove(0,0)

    def girarIzquierda(self, vel):
        '''
        Función que hace rotar al robot sobre sí mismo hacia la izquierda a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de giro del robot (máximo 255)
        '''

        self.doMove(vel, -vel)

    def girarDerecha(self, vel):
        '''
        Función que hace rotar al robot sobre sí mismo hacia la derecha a una velocidad dada como parámetro.

        @type vel: entero
        @param vel: velocidad de giro del robot (máximo 255)
        '''

        self.doMove(-vel, vel)

    def decisor_vel (self, vel):
        #decisor de velocidades (escala)
        v = 0
        if (vel<-200):
            v = -255
        elif (vel>=-200 and vel<-150):
            v = -200
        elif (vel>=-150 and vel<-100):
            v = -150
        elif (vel>=-100 and vel<-50):
            v = -100
        elif (vel>=-50 and vel<0):
            v = -50
        elif (v==0):
            v = 0
        elif (vel>0 and vel<=50):
            v = 50
        elif (vel>50 and vel<=100):
            v = 100
        elif (vel>100 and vel<=150):
            v = 150
        elif (vel>150 and vel<=200):
            v = 200
        elif (vel>200):
            v = 255

        return v
    def mover(self, velV, velW):
        'Función que hace avanzar y girar a la vez;velV: velocidad lineal, velW velocidad de giro'
        velV = self.decisor_vel(velV)
        velW = self.decisor_vel(velW)
        #velocidades de avance y giro
        if (velW == 0):
            self.doMove(velV, velV) #avanzar recto (o retroceder con vel negativa)
        elif (velV == 0): #girar pero no avanzar
            if (velW < 0): #girar derecha
                self.doMove(velW, 0)
            else: #girar a la izquierda (hacia delante)
                self.doMove(0, -velW)
        else: #girar y avanzar (o retroceder)
            if velV > 0: #avanzar y girar
                if velW > 0: #girar a la izquierda
                    self.doMove(velV, velW+velV)
                else: #girar a la derecha
                    self.doMove(velV-velW, velV)
            else: #retroceder y girar
                if velW < 0: #girar a la derecha
                    self.doMove(velV+velW, velV)
                elif velW > 0: #girar izquierda
                    self.doMove(velV, velV-velW)




    def update(self):

        self.requestUltrasonicSensor(122,3, _cb_ultrasonido)
        self.requestPotentiometerSensor(125,4,_cb_potenciometro)
        self.requestSoundSensor(124,4, _cb_sonido)
        self.requestLineFollower(123,2, _cb_irSiguelineas)
        self.requestButtonOnBoard(128, _cb_boton)
        self.requestLightOnBoard(130, _cb_luzPLaca)
        self.requestIROnBoard(1, _cb_irr)

    def quienSoy(self):
        print ("Yo soy un robot real realMBot")

    @property
    def tipo(self):
        return self._tipo

    @tipo.setter
    def tipo(self, valor):
        self._tipo = valor

def _cb_boton(bot, dat):
    #guarda en la variable _boton el valor recibido de los pusadores
    bot.lockBut.acquire()
    bot._boton = dat
    bot.lockBut.release()

def _cb_luzPLaca(bot, dat):
    #guarda en la variable _luzPlaca el valor recibido del sensor de luz de la placa
    bot.lockLP.acquire()
    bot._luzPlaca = dat
    bot.lockLP.release()

def _cb_ultrasonido(bot, dat):
    #guarda en la variable _ultrasonido el valor recibido del ultrasonido
    bot.lockUS.acquire()
    bot._ultrasonido = dat
    bot.lockUS.release()

def _cb_irr(bot, dat):
    bot.lockIRR.acquire()
    bot._irr = dat
    bot.lockIRR.release()

def _cb_irSiguelineas(bot, dat):
    bot.lockSL.acquire()
    bot._irsiguelineas = dat
    bot.lockSL.release()

def _cb_sonido(bot, dat):
    #guarda en la variable _ultrasonido el valor recibido del sensor de sonid
    bot.lockSon.acquire()
    bot._sonido = dat
    bot.lockSon.release()

def _cb_potenciometro(bot, dat):
    #guarda en la variable _ultrasonido el valor recibido del potenciometro
    bot.lockPot.acquire()
    bot._potenciometro = dat
    bot.lockPot.release()
