#(c) PyBoKids

from realMBot import *

if __name__ == '__main__':

    robot = realMBot("/dev/ttyUSB0")
    lista_valores_mando = robot.valoresMandoPosibles()
    print ("Valores del mando: ")
    print (lista_valores_mando)

    while(1):
        valor_mando = robot.leerMandoIR()
        if valor_mando != "null":
            print (valor_mando)
        time.sleep(0.2)
