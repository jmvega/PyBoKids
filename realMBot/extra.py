#(c) PyBoKids

# -*- coding: utf-8 -*-

def dec2Bin (num, l):
	"""Devuelve un numero en formato de bits.

	los devuelve de bit menos significativo a mas significativo.
	Por ejemplo:
		dec2Bin(3,3) devuelve [1,1,0] en lugar de [0,1,1]
	
	Parametros:
	  num -> numero a convertir
	  l -> numero de bits que deseas

	
	>>> dec2Bin(3,3)
	[1, 1, 0]

	"""
	form = '{0:0'+str(l)+'b}'
	s = form.format(num)
	ls = list(s)
	bits = []
	for i in reversed(ls):
		bits.append(int(i))
	return bits
