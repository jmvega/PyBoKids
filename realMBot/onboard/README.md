1. Download the source from the git https://github.com/Makeblock-official/Makeblock-Libraries

2. copy the makeblock folder to your arduino default library. Your Arduino library folder should now look like this (on Windows): [arduino installation directory]\libraries\makeblock\src (on MACOS): [arduino Package Contents]\contents\Java\libraries\makeblock\src

3. Open the Arduino Application. (If it's already open, you will need to restart it to see changes.)

4. Open mbot_PyBoKids.ino and upluado to mbot selecting "arduino uno" board
