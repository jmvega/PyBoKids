# -*- coding: utf-8 -*-

#Botones
BOTON_NO_PULSADO = 0
BOTON_CEN = 1
BOTON_IZQ = 2
BOTON_SUP = 3
BOTON_DER = 4
BOTON_INF = 5



class ArduinoJdeRobot ():
	"""API JdeRobot para robots Arduino.

	Esta clase sólo funciona como plantilla a implementar con cada uno de los robots Arduino. No se puede usar como tal.
	Cada vez que se llame a un método que no tenga implementación en el robot que se está usando imprimirá un mensaje avisándolo

	"""

	def __init__(self, model):
		self.model = model
		print("Modelo de Robot: " + str(self.model))
		

	def encenderLed(self, *args):
		print(self.model + " no usa este metodo")
		
	def apagarLed(self, *args):
		print(self.model + " no usa este metodo")

	def leerBoton(self, *args):
		print(self.model + " no usa este metodo")
			
	def leerPotenciometro(self, *args):
		print(self.model + " no usa este metodo")

	def playTono(self, *args):
		print(self.model + " no usa este metodo")