from lib.mBot import *
#from multiprocessing import freeze_support
from time import sleep
if __name__ == '__main__':
	#freeze_support()
	bot = mBot()
	bot.startWithSerial("/dev/ttyUSB0")
	#bot.startWithHID()
	tones ={"ZERO":0,"C3":131,"D3":147,"E3":165,"F3":175,"G3":196,"A3":220,"B3":247,
				"C4":262,"D4":294,"E4":330,"F4":349,"G4":392,"G4S":415,"A4":440,"A4S":455,"B4":494,
				"C5":523,"C5H":554,"D5":587,"D5S":622,"E5":659,"F5":698,"F5S":740,"G5":784,"G5S":830,"A5":880,"B5":988}
	#music = ["E4","E4","F4","G4","G4","F4","E4","D4","C4","C4","D4","E4","E4","D4","D4"];
	im0 = ["A4","A4","A4","F4","C5","A4","F4","C5","A4"]
	im1 = ["E5","E5","E5","F5","C5","G4S","F4","C5","A4"]
	it0 = [500,500,500,350,150,500,350,150,650]
	it1 = [500,500,500,350,150,500,350,150,650]

	test = ["E5","E5","E5","F5","C5","G4S","F4","C5","A4"]
	test_t = [500,500,500,350,150,500,350,150,650]

	while(1):
		try:	
			#for i in range(0,len(it0)):
			#	bot.doBuzzer(tones[im0[i]],it0[i])
			#	sleep(it0[i])
			#bot.doBuzzer(0,1000)
			#for i in range(0,len(it1)):
			#	bot.doBuzzer(tones[im1[i]],it1[i])
			#	bot.doBuzzer(0,20)
			#bot.doBuzzer(0,1000)

			bot.doBuzzer(tones["A4"],500)
			print("A4", end=" ")
			bot.doBuzzer(tones["A4"],500)
			print("A4", end=" ")
			bot.doBuzzer(tones["A4"],500)
			print("A4", end=" ")
			bot.doBuzzer(tones["F4"],350)
			print("F4", end=" ")
			bot.doBuzzer(tones["C5"],150)
			print("C5", end=" ")
			bot.doBuzzer(tones["A4"],500)
			print("A4", end=" ")
			bot.doBuzzer(tones["F4"],350)
			print("F4", end=" ")
			bot.doBuzzer(tones["C4"],150)
			print("C5", end=" ")
			bot.doBuzzer(tones["A4"],650)
			print("A4", end=" ")

		except Exception as ex:
			bot.doBuzzer(0)
			print (str(ex))
		bot.doBuzzer(0)
		sleep(3)
