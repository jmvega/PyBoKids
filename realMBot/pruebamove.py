#(c) PyBoKids

import sys
import time
from realMBot import *

def signal_handler(sig, frame):
	print('Has pulsado Ctrl+C')
	sys.exit(0)

robot = realMBot("/dev/ttyUSB0") # Conectamos con el robot
vel_v = 100
vel_w = 50


robot.mover(vel_v, 0)
print("avanzar solo")
time.sleep(2)
robot.mover(-vel_v, 0)
print("retroceder solo")
time.sleep(2)
robot.mover(vel_v, vel_v)
print ("girar derecha y avanzar")
time.sleep(2)
robot.mover(0, vel_w)
print ("solo girar derecha")
time.sleep(2)
robot.mover(0, -vel_w)
print("solo girar izquierda")
robot.mover(0,0)
