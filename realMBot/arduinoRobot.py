#(c) PyBoKids

# -*- coding: utf-8 -*-

from time import sleep

from .arduinoPyBoKids import arduinoPyBoKids, BOTON_CEN, BOTON_DER, BOTON_IZQ, BOTON_SUP, BOTON_INF, BOTON_NO_PULSADO

from PyMata.pymata import PyMata
from .multiplexer import DigitalMultiplexer



######### Pines Placa Control ################## 
BOTON_PIN = 0 # Analogico

# Multiplexor (Z -> entrada/salida)
# Digitales
A = 6
B = 11
C = 12
D = 13
Z = 4

# Zumbador (Digital)
BUZZ = 5
	
# Potenciometro (Analogico)
POT = 5


class ArduinoRobot (arduinoPyBoKids, PyMata):
	"""Implementacion del API arduinoPyBoKids para el robot: Arduino Robot.

	Permite comunicarse con el robot Arduino Robot usando PyMata. Hereda de arduinoPyBoKids y PyMata


	Pines Placa Control(D digital, A analogico):

	D0 - 
	D1 - 
	D2 - Conexion entre placas
	D3 - 
	D4 - salida/entrada del multiplexor
	D5 - Zumbador
	D6 - A del multiplexor
	D7 - 
	D8 - 
	D9 - 
	D10 - 
	D11 - B del multiplexor 
	D12 - C del multiplexor
	D13 - D del multiplexor

	A0 - Botones
	A1 - 
	A2 - 
	A3 -  
	A4 -
	A5 - Potenciometro


	valores aproximados leidos del pin para cada boton:
	BOTON_IZQ ~ 0
	BOTON_DER ~ 504
	BOTON_CEN ~ 744
	BOTON_SUP ~ 328
	BOTON_INF ~ 143
	BOTON_NO_PULSADO ~ 1023

	"""

	def __init__(self,port):
		"""Contructor.

		Parametros:
		  port -> puerto serie donde está conectado el robot

		"""

		
		arduinoPyBoKids.__init__(self, "Arduino Robot")
		PyMata.__init__(self, port)

		selec = [A,B,C,D]
		self._multiplexer = DigitalMultiplexer(selec, Z, self)

		self.i2c_config(0,PyMata.DIGITAL, 3, 2)

		self._boton = 1023 # Valor que devuelve el robot cuando no se pulsa ningun boton
		self._potenciometro = 0


		self.set_pin_mode(BOTON_PIN, PyMata.INPUT, PyMata.ANALOG, self._cb_boton) # Activamos botones con su callback
		self.set_pin_mode(POT, PyMata.INPUT, PyMata.ANALOG, self._cb_pot) # activamos potenciometro con su callback
		self.set_pin_mode(BUZZ, PyMata.OUTPUT, PyMata.DIGITAL)# activamos el Zumbador

	def encenderLed(self, *args):
		"""Enciende el LED indicado.

		Enciende el LED que está en el pin indicado del multiplexor. 

		Parametros:
		  args[0] -> pin indicado

		"""
		num = args[0]
		if (self._multiplexer.isModeRead()):
			self._multiplexer.changeMode()

		self._multiplexer.setValueAt(num, True)

	def apagarLed(self, *args):
		"""Apaga el LED indicado.

		Apaga el LED que está en el pin indicado del multiplexor. 

		Parametros:
		  args[0] -> pin indicado

		"""
		num = args[0]
		if (self._multiplexer.isModeRead()):
			self._multiplexer.changeMode()

		self._multiplexer.setValueAt(num, False)

	def leerBoton(self, *args):
		"""Devuelve la ultima pulsacion de los botones.

		Parmetros:
		  No utiliza parametros 

		valores aproximados leidos del pin para cada boton:
		BOTON_IZQ ~ 0
		BOTON_DER ~ 504
		BOTON_CEN ~ 744
		BOTON_SUP ~ 328
		BOTON_INF ~ 143
		BOTON_NO_PULSADO ~ 1023

		"""
		if (self._boton > 1000):
			return BOTON_NO_PULSADO
		elif(self._boton > 700):
			return BOTON_CEN
		elif(self._boton > 500):
			return BOTON_DER
		elif(self._boton > 300):
			return BOTON_SUP
		elif(self._boton > 100):
			return BOTON_INF
		else:
			return BOTON_IZQ

			
	def leerPotenciometro(self, *args):
		"""Devuelve el valor del potenciometro.

		Parmetros:
		  No utiliza parametros 

		"""
		return self._potenciometro

	def playTono(self, *args):
		""" Reproduce un tono el tiempo indicado en segundos.
		
		El tiempo se limita a 5s como maximo por seguridad
		El programa se bloquea mientras se emite el tono (se usa sleep para controlar el tiempo)

		Parametros:
		  args[0] -> tiempo en segundos

		"""
		tiempo = args[0]
		flag = True
		if tiempo > 5:
			tiempo = 5
		while True:
			if flag :
				self.digital_write(BUZZ, True)
				flag = False
				sleep(tiempo)
			else:
				self.digital_write(BUZZ, False)
				return

	def _cb_boton(self, dat):
		"""Callback de los pulsadores.

		guarda en la variable _boton el valor recibido de los pulsadores.
		
		Parametros:
		  dat -> valor recibido

		"""
		self._boton = dat[2]

	def _cb_pot(self, dat):
		"""Callback del potenciometro.

		guarda en la variable _pot el valor recibido del potenciometro.
		
		Parametros:
		  dat -> valor recibido

		"""
		self._potenciometro = dat[2]
