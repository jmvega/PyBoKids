# PyBoKids: Framework for teaching Robotics

PyBokids includes a complete **Python-based middleware** for teaching Robotics through programming different Arduino-based robots such as Arduino board or mBot, even a simulated one. It also provides a collection of exercises and a suggested academic program to be followed in a practical way. The students program their solutions in **Python language**. Each exercise is composed of:

1. Gazebo configuration files (just in case of using a simulated mBot).
2. **myAlgorithm.py** file, which hosts the student's code.
3. The **PyBoKids API**, which is imported from the file described in (2).

All the previous dependencies are already provided in the **PyBoKids framework**, the students just have to develop their code on the file (2) which already has a **template**. The students may use there an existing simple *Python* *API* to access to sensor readings and actuator commands. Summarizing, to develop their solution, the students have to edit that template file and add their code using their favorite text editor.

